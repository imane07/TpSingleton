package user;

import java.util.Scanner;

public class User{
	private String nom;
	private String prenom;
	
	/**
	 * Instantion priv� de la Classe User
	 */
	private static User user = new User();
	
	/**
	 * Constructeur Priv� du User
	 */
	private User() {
		
	}
	
	
	protected String getNom() {
		return nom;
	}
	
	protected void setNom(String nom) {
		this.nom = nom;
	}
	
	protected String getPrenom() {
		return prenom;
	}
	
	protected void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public static User getUser() {
		return user;
	}
	
	public static void setUser(User user) {
		User.user = user;
	}

	
	
}
