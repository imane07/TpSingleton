package user;

import java.util.Scanner;

public class StaticUser {
	
	private String nom;
	private String prenom;
	
	public StaticUser() {
		
	}
	
	public StaticUser(String unom, String uprenom) {
		this.nom = unom;
		this.prenom = uprenom;
	}
	
	
		
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
